import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import { connect } from "react-redux";
import chatActions from '../actions/chat.actions';

@connect()

export default class Home extends React.Component {

  state =
  {
    username: "",
    room: ""
  }

  handleJoin= () =>
  {
    const { username, room } = this.state;
    const { navigation, dispatch } = this.props;

    dispatch(chatActions.join(username, room));

    navigation.push('Chat', {username, room})
  }

  render() {
    const { username, room } = this.state;
    const { navigation } = this.props;

    return (
      <View style={styles.root}>
        <View style={styles.formControl}>
          <Text style={styles.label}>
            Username
          </Text>
          <TextInput
            style={styles.textInput}
            placeholder="Type your username..."
            onChangeText={(username) => this.setState({username})}
            value={username}
          />
        </View>

        <View style={styles.formControl}>
          <Text style={styles.label}>
            Room
          </Text>
          <TextInput
            style={styles.textInput}
            placeholder="Room name..."
            onChangeText={(room) => this.setState({room})}
            value={room}
          />
        </View>

        <View style={styles.formControl}>
          <Button
            title="Go to chat"
            onPress={this.handleJoin}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 24
  },
  textInput: {
    borderWidth: 1,
    borderColor: '#555',
    width: '100%',
    padding: 8
  },
  label: {
    width: '100%',
    textAlign: 'left',
    marginBottom: 5,
    color: '#555'
  },
  formControl: {
    marginVertical: 8,
    width: '100%'
  }
});